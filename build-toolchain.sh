#!/bin/bash

source "config/config.cfg"

#
# Make binutils and gcc for compiling linux
#
#   NOTE1: buildroot compiles its own toolchains (can we change this?)
#   NOTE2: firmware (iop modules) needs the ps2sdk toolchain
#
cd toolchain
	# Change to another binutils branch?
#	cd "${BINUTILS_DIR}"
#	git checkout ps2-v2.24 || exit -1
#	cd ..

	# Change to another gcc branch?
#	cd "${GCC_DIR}"
#	git checkout ps2-v4.9.0 || exit -1
#	cd ..

	# Remove the next line to rebuild the entire toolchain
#	rm -rf build
	if [ ! -d build ]; then
		mkdir build || exit -1
	fi

	cd build
		# Remove the next line to rebuild binutils
#		rm -rf binutils
		if [ ! -d binutils ]; then
			mkdir binutils || exit -1
			cd binutils
			# binutils 2.24 fails to build using newer versions of gcc
			# Add CFLAGS=-Wno-error to workaround the new warnings
			CFLAGS=-Wno-error ../../binutils/configure --program-prefix="${TOOLCHAIN_PROGRAMPREFIX}" --target=${TOOLCHAIN_TARGET} --enable-targets=${TOOLCHAIN_TARGET} --enable-shared --enable-plugins || exit -1
			make -j 5 || exit -1
			sudo make install || exit -1
			cd ..
		fi

		# Remove the next line to rebuild gcc
#		rm -rf "${GCC_DIR}"
		if [ ! -d gcc ]; then
			mkdir gcc || exit -1
			cd gcc
			../../gcc/configure --program-prefix="${TOOLCHAIN_PROGRAMPREFIX}" --target=${TOOLCHAIN_TARGET} --enable-languages=c --disable-nls --disable-shared --disable-libssp --disable-libmudflap --disable-threads --disable-libgomp --disable-libquadmath --disable-target-libiberty --disable-target-zlib --without-ppl --without-cloog --with-headers=no --disable-libada --disable-libatomic || exit -1
			make -j 5 || exit -1
			sudo make install || exit -1
			cd ..
		fi
	cd ..
cd ..
